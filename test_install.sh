# use this to build locally - runs through the whole test_build_buildpkg process.
# pass in the bulid number - needs to be supplied since normally supplied by CI pipeline.

# do this before setting -eou pipefail to avoid unbound variable error.
if [[ -z ${1} ]]; then
   echo "Please supply a build number"
   exit 1
fi


set -eou pipefail # needed so script exits on errors.
SECONDS=0


# This line reads in all the variables in the gitlab CI file and creates the appropriate env variables.
# This allows us to define the vars once in the yml file
# and then run the build process locally using the same vars
# see https://unix.stackexchange.com/questions/539009/export-environment-variables-parsed-from-yaml-text-file
. <(sed -nr '/variables:/,$ s/  ([A-Z_]+): (.*)/\1=\2/ p' vars.yml)
# We set the full version here to whatever is passed in
# This is done after we read in the yaml file so we overwrite whatever was there
export FULL_VERSION=$VERSION.$1
#echo "Install check for Version $FULL_VERSION"
#echo "Installing Python - needed for PyTest"
#curl "https://www.python.org/ftp/python/3.12.0/python-3.12.0-amd64.exe" --output python312_installer.exe
#./python312_installer.exe -quiet
#alias python="/c/Users/SAS/AppData/Local/Programs/Python/Python312/python.exe"
#alias pip="/c/Users/SAS/AppData/Local/Programs/Python/Python312/Scripts/pip.exe"
#echo "Installing Requirements.txt"
#"/c/Users/SAS/AppData/Local/Programs/Python/Python312/Scripts/pip.exe" -q install -r requirements.txt
echo "Installing Package using VIPM"
PKG_FILE=$(ls -t "$PKG_BUILD_DIR" | head -1)
PKG_PATH="$PKG_BUILD_DIR"/"$PKG_FILE"
g-cli --timeout "$GCLI_LV_TIMEOUT" --lv-ver "$GCLI_LV_VERSION" --arch "$GCLI_LV_ARCH" clearlvcache # for G-CLI 3.0 and later can replace with ClearCache
#g-cli --timeout "$GCLI_LV_TIMEOUT" --lv-ver "$GCLI_LV_VERSION" --arch "$GCLI_LV_ARCH" vipc -- "$VIPC_PATH" -t "$VIPC_TIMEOUT" -v "$VIPC_LV_VERSION"
g-cli --timeout "$GCLI_LV_TIMEOUT" --lv-ver "$GCLI_LV_VERSION" --arch "$GCLI_LV_ARCH" vip -- --lv-version "$VIPC_LV_VERSION" --local-vip-file "$PKG_PATH"  
#echo "Running Pytest"
#"/c/Users/SAS/AppData/Local/Programs/Python/Python312/python.exe" -m pytest --capture=tee-sys -o junit_logging=all --junit-xml=pytest_report.xml

echo "Total Script Time: $SECONDS"
