# Quick VIAN project
[![Image](https://gitlab.com/sas-blog/QuickVIAN/-/badges/release.svg)](https://gitlab.com/sas-blog/QuickVIAN/-/releases) [![Image](https://gitlab.com/sas-blog/QuickVIAN/badges/main/pipeline.svg)](https://gitlab.com/sas-blog/QuickVIAN/-/pipelines) [![Image](https://www.vipm.io/package/sas_workshops_lib_quick_vian/badge.svg?metric=installs)](https://www.vipm.io/package/sas_workshops_lib_quick_vian/) [![Image](https://www.vipm.io/package/sas_workshops_lib_quick_vian/badge.svg?metric=stars)](https://www.vipm.io/package/sas_workshops_lib_quick_vian/)




This project is a QuickDrop shortcut (ctl + Q) designed to allow you to run VI Analyzer only on changed files. It knows which files have changed by running git diff.

# Getting Started

1. Get the latest released package [here](https://gitlab.com/sas-blog/QuickVIAN/-/releases) and install it.
1. Open your project (it must be in Git in order for the tool to work).
1. Open the tool using the QuickDrop shortcut. This defaults to ctl + q.
1. Decide what you want to diff against (see the guidelines for suggestions - the default just collects changed files since your last commit). Then press Forward.
   ![](images/initial.png)

